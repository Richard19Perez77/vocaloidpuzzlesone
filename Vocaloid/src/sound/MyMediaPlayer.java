package sound;

import java.io.IOException;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;

import common.CommonVariables;
import common.CommonWork;

import data.Data;

/**
 * 
 * A class to extend Media Player and implement handling interfaces. I also
 * started implementing the ability to handle the sound changes due to incoming
 * notification sounds like phone or message alerts *
 * 
 * Should handle errors with headphones becoming unplugged and media player
 * states with opening and closing of application.
 * 
 * @author Rick
 * 
 */
public class MyMediaPlayer extends MediaPlayer implements
		MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
		AudioManager.OnAudioFocusChangeListener,
		MediaPlayer.OnCompletionListener {

	CommonVariables cv = CommonVariables.getInstance();
	CommonWork cw = CommonWork.getInstance();

	public MediaPlayer mediaPlayer;
	float actualVolume, maxVolume;
	Uri path = Uri.parse(Data.PATH + Data.TRACK_01);
	AudioManager am;
	int result;

	enum States {
		Idle, Initialized, Prepared, Started, Preparing, Stopped, Paused, End, Error, PlaybackCompleted
	}

	States currentState;

	public void init() {
		am = (AudioManager) cv.context.getSystemService(Context.AUDIO_SERVICE);
		result = am.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
				AudioManager.AUDIOFOCUS_GAIN);
		mediaPlayer = new MediaPlayer();
		mediaPlayer.reset();
		mediaPlayer.setOnPreparedListener(this);
		mediaPlayer.setOnErrorListener(this);
		mediaPlayer.setOnCompletionListener(this);
		currentState = States.Idle;
	}

	public void start() {
		if (cv.playMusic) {
			if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
				if (currentState != States.Idle)
					init();
				if (currentState != States.Preparing) {
					try {
						mediaPlayer.setDataSource(cv.context, path);
						currentState = States.Initialized;
						mediaPlayer
								.setAudioStreamType(AudioManager.STREAM_MUSIC);
						mediaPlayer.setVolume(cv.volume, cv.volume);
						mediaPlayer.prepareAsync();
						currentState = States.Preparing;
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SecurityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	@Override
	public void onPrepared(MediaPlayer player) {
		// check for option to play music and resume last position
		if (currentState == States.Preparing) {
			currentState = States.Prepared;
			if (cv.playMusic) {
				if (cv.currentSoundPosition >= 0) {
					mediaPlayer.seekTo(cv.currentSoundPosition);
				}
				if (currentState != States.End && !player.isPlaying()) {
					player.start();
					currentState = States.Started;
				}
			}
		}
	}

	@Override
	public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
		currentState = States.Error;
		mediaPlayer.reset();
		start();
		return true;
	}

	@Override
	public void onAudioFocusChange(int focusChange) {
		// Handle audio lowering and raising for other phone sounds
		switch (focusChange) {
		case AudioManager.AUDIOFOCUS_GAIN:
			// resume play back
			if (mediaPlayer == null)
				init();
			else if (!mediaPlayer.isPlaying()) {
				start();
			}
			break;
		case AudioManager.AUDIOFOCUS_LOSS:
			// lost focus for an unbounded amount of time. stop and release
			pause();
			break;
		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
			// lost focus for a short time, but we have to stop play back.
			if (mediaPlayer != null && mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
				currentState = States.Paused;
				cv.currentSoundPosition = mediaPlayer.getCurrentPosition();
			}
			break;
		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
			if (mediaPlayer != null) {
				setNewVolume(0.1f);
			}
			break;
		}
	}

	public void resume() {
		// media player should have been destroyed in last pause
		init();
		start();
	}

	public void pause() {
		if (am != null)
			am.abandonAudioFocus(this);
		if (currentState != States.End && mediaPlayer != null) {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
				currentState = States.Paused;
			}
			if (currentState == States.Started || currentState == States.Paused) {
				mediaPlayer.stop();
				currentState = States.Stopped;
			}
			mediaPlayer.release();
			currentState = States.End;
			mediaPlayer = null;
		}
	}

	public void destroy() {
		// a final check when the app closes down for good
		if (mediaPlayer != null) {
			mediaPlayer.release();
			currentState = States.End;
			mediaPlayer = null;
		}
	}

	public void setNewVolume(Float setVolume) {
		if (currentState != States.End && mediaPlayer.isPlaying()) {
			mediaPlayer.setVolume(setVolume, setVolume);
		}
	}

	public void cleanUp() {
		// a final check when the app closes down for good
		if (mediaPlayer != null) {
			mediaPlayer.release();
			currentState = States.End;
			mediaPlayer = null;
		}
	}

	public void toggleMusic() {
		if (cv.playMusic) {
			cv.playMusic = false;
			cw.showToast(cv.context, "Music off");
			pause();
		} else {
			cv.playMusic = true;
			cw.showToast(cv.context, "Music on/restarted");
			resume();
		}
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		currentState = States.PlaybackCompleted;
		cv.currentSoundPosition = 0;
		start();
	}

	public void onStop() {
		if (mediaPlayer != null) {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
				currentState = States.Paused;
				cv.currentSoundPosition = mediaPlayer.getCurrentPosition();
			}
		}
	}
}