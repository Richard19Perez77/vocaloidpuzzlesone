package common;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class CommonWork {

	private volatile static CommonWork instance;
	Toast toast;

	public static CommonWork getInstance() {
		if (instance == null)
			synchronized (CommonWork.class) {
				if (instance == null)
					instance = new CommonWork();
			}
		return instance;
	}

	public void showToast(Context cont, String message) {
		// create if not, or set text to it
		if (toast == null) {
			toast = Toast.makeText(cont, message, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 0);
		}
		if (!toast.getView().isShown()) {
			toast.setText(message);
			toast.show();
		} else {
			toast.cancel();
			toast.setText(message);
			toast.show();
		}
	}
}