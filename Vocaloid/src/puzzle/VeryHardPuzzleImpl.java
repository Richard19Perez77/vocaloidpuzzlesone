package puzzle;

import java.util.Date;

import surface.PuzzlePiece;
import android.graphics.Bitmap;
import android.view.MotionEvent;

import common.CommonVariables;

import data.Data;

/**
 * 
 * A class with very hard implementation is a 5 x 5 grid.
 * 
 * @author Rick
 * 
 */
public class VeryHardPuzzleImpl implements Puzzle {

	public int PIECES = 25;
	int bitmapWd5;
	int bitmapHd5;

	PuzzleWork puzzleWork;
	CommonVariables common = CommonVariables.getInstance();

	public VeryHardPuzzleImpl() {
		puzzleWork = new PuzzleWork();
	}

	public void getNewImageLoadedScaledDivided(Thread thread) {
		puzzleWork.newImageComplete = false;
		if (thread != null && thread.isAlive())
			thread.interrupt();

		thread = new Thread() {
			@Override
			public void run() {

				while (!puzzleWork.newImageComplete) {
					// fill with all valid numbers
					if (common.imagesShown.isEmpty())
						for (int i = 0; i < Data.PICS.length; i++)
							common.imagesShown.add(i);

					// get new index value from remaining images
					puzzleWork.index = common.rand.nextInt(common.imagesShown
							.size());
					// get the value at that index for new image
					common.currentPuzzleImagePosition = common.imagesShown
							.get(puzzleWork.index);
					// remove from list to prevent duplicates
					common.imagesShown.remove(puzzleWork.index);

					common.image = common.decodeSampledBitmapFromResource(
							common.res,
							Data.PICS[common.currentPuzzleImagePosition],
							common.screenW, common.screenH);
					common.image = Bitmap.createScaledBitmap(common.image,
							common.screenW, common.screenH, true);

					puzzleWork.newImageComplete = divideBitmap();

					if (puzzleWork.newImageComplete) {
						resetTimer();
						common.errorLoading = false;
						common.solved = false;
						common.imageReady = true;
						common.mySoundPool.playChimeSound();
						System.gc();
					} else {
						common.errorLoading = true;
					}
				}
			}
		};
		thread.start();
	}

	public boolean divideBitmap() {
		common.initDivideBitmap(PIECES);

		puzzleWork.isRandom = false;
		while (!puzzleWork.isRandom) {
			for (int i = 0; i < common.slotOrder.length; i++) {
				int switchIndex = common.rand.nextInt(common.slotOrder.length);
				int tempValue = common.slotOrder[i];
				common.slotOrder[i] = common.slotOrder[switchIndex];
				common.slotOrder[switchIndex] = tempValue;
				if (i != switchIndex) {
					puzzleWork.isRandom = true;
				}
			}
		}

		puzzleWork.piecesComplete = 0;

		common.evenlySplit = false;
		while (!common.evenlySplit) {
			int w = common.image.getWidth();
			int h = common.image.getHeight();
			bitmapWd5 = w / 5;
			bitmapHd5 = h / 5;
			int x, y;
			for (int i = 0; i < PIECES; i++) {
				if (i < 5) {
					y = 0;
				} else if (i < 10) {
					y = bitmapHd5;
				} else if (i < 15) {
					y = bitmapHd5 * 2;
				} else if (i < 20) {
					y = bitmapHd5 * 3;
				} else {
					y = bitmapHd5 * 4;
				}

				x = (i % 5) * bitmapWd5;

				if (common.puzzlePieces[i].bitmap != null)
					common.puzzlePieces[i].bitmap.recycle();

				common.puzzlePieces[i].bitmap = null;
				common.puzzlePieces[i].bitmap = Bitmap.createBitmap(
						common.image, x, y, bitmapWd5, bitmapHd5);

				common.puzzlePieces[i].px = x;
				common.puzzlePieces[i].px2 = x + bitmapWd5;

				common.puzzlePieces[i].py = y;
				common.puzzlePieces[i].py2 = y + bitmapHd5;

				common.puzzleSlots[i].sx = x;
				common.puzzleSlots[i].sx2 = x + bitmapWd5;

				common.puzzleSlots[i].sy = y;
				common.puzzleSlots[i].sy2 = y + bitmapHd5;

				common.puzzleSlots[i].puzzlePiece = common.puzzlePieces[i];
				common.puzzleSlots[i].slotNum = common.puzzleSlots[i].puzzlePiece.pieceNum = i;

				puzzleWork.piecesComplete++;

			}
			common.evenlySplit = true;
			common.image.recycle();
			common.image = null;
		}

		common.jumblePicture();

		if (puzzleWork.piecesComplete == PIECES)
			return true;

		return false;
	}

	@Override
	public boolean divideBitmapFromPreviousPuzzle() {
		common.initPrevDivideBitmap(PIECES);

		// re do if the image didn't split correctly
		common.evenlySplit = false;
		puzzleWork.piecesComplete = 0;

		while (!common.evenlySplit) {
			int w = common.image.getWidth();
			int h = common.image.getHeight();
			bitmapWd5 = w / 5;
			bitmapHd5 = h / 5;
			int x, y;
			for (int i = 0; i < PIECES; i++) {
				if (i < 5) {
					y = 0;
				} else if (i < 10) {
					y = bitmapHd5;
				} else if (i < 15) {
					y = bitmapHd5 * 2;
				} else if (i < 20) {
					y = bitmapHd5 * 3;
				} else {
					y = bitmapHd5 * 4;
				}

				x = (i % 5) * bitmapWd5;

				if (common.puzzlePieces[i].bitmap != null)
					common.puzzlePieces[i].bitmap.recycle();

				common.puzzlePieces[i].bitmap = null;
				common.puzzlePieces[i].bitmap = Bitmap.createBitmap(
						common.image, x, y, bitmapWd5, bitmapHd5);

				common.puzzlePieces[i].px = x;
				common.puzzlePieces[i].px2 = x + bitmapWd5;

				common.puzzlePieces[i].py = y;
				common.puzzlePieces[i].py2 = y + bitmapHd5;

				common.puzzleSlots[i].sx = x;
				common.puzzleSlots[i].sx2 = x + bitmapWd5;

				common.puzzleSlots[i].sy = y;
				common.puzzleSlots[i].sy2 = y + bitmapHd5;

				// cv.puzzleSlots[i].puzzlePiece = cv.puzzlePieces[i];
				common.puzzleSlots[i].slotNum = common.puzzleSlots[i].puzzlePiece.pieceNum = i;

				puzzleWork.piecesComplete++;

			}
			common.evenlySplit = true;
			common.image.recycle();
			common.image = null;
		}

		// set puzzle piece to a new slot by creating separate puzzle pieces and
		// reassign individually
		boolean correctlyReassembled = false;
		while (!correctlyReassembled) {
			// use saved slot list to sort
			for (int toSlot = 0; toSlot < common.slotOrder.length; toSlot++) {

				// get new slot to take piece from and place into correct slot
				int fromSlot = common.slotOrder[toSlot];
				PuzzlePiece pieceA = common.puzzlePieces[fromSlot];

				common.puzzleSlots[toSlot].puzzlePiece = pieceA;
				common.puzzleSlots[toSlot].puzzlePiece.px = common.puzzleSlots[toSlot].sx;
				common.puzzleSlots[toSlot].puzzlePiece.py = common.puzzleSlots[toSlot].sy;
				common.puzzleSlots[toSlot].puzzlePiece.px2 = common.puzzleSlots[toSlot].sx2;
				common.puzzleSlots[toSlot].puzzlePiece.py2 = common.puzzleSlots[toSlot].sy2;
				common.puzzleSlots[toSlot].puzzlePiece.pieceNum = fromSlot;
			}

			correctlyReassembled = true;
			for (int j = 0; j < common.puzzleSlots.length; j++) {
				int a = common.puzzleSlots[j].puzzlePiece.pieceNum;
				int b = common.slotOrder[j];
				if (a != b) {
					correctlyReassembled = false;
				}
			}
		}
		if (puzzleWork.piecesComplete == PIECES)
			return true;

		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int newx = (int) event.getX();
		int newy = (int) event.getY();

		// find the piece that was pressed down onto
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			common.movingPiece = false;
			if (newx < bitmapWd5) {
				if (newy < bitmapHd5) {
					common.currPieceOnTouch = 0;
				} else if (newy < bitmapHd5 * 2) {
					common.currPieceOnTouch = 5;
				} else if (newy < bitmapHd5 * 3) {
					common.currPieceOnTouch = 10;
				} else if (newy < bitmapHd5 * 4) {
					common.currPieceOnTouch = 15;
				} else {
					common.currPieceOnTouch = 20;
				}
			} else if (newx < bitmapWd5 * 2) {
				if (newy < bitmapHd5) {
					common.currPieceOnTouch = 1;
				} else if (newy < bitmapHd5 * 2) {
					common.currPieceOnTouch = 6;
				} else if (newy < bitmapHd5 * 3) {
					common.currPieceOnTouch = 11;
				} else if (newy < bitmapHd5 * 4) {
					common.currPieceOnTouch = 16;
				} else {
					common.currPieceOnTouch = 21;
				}
			} else if (newx < bitmapWd5 * 3) {
				if (newy < bitmapHd5) {
					common.currPieceOnTouch = 2;
				} else if (newy < bitmapHd5 * 2) {
					common.currPieceOnTouch = 7;
				} else if (newy < bitmapHd5 * 3) {
					common.currPieceOnTouch = 12;
				} else if (newy < bitmapHd5 * 4) {
					common.currPieceOnTouch = 17;
				} else {
					common.currPieceOnTouch = 22;
				}
			} else if (newx < bitmapWd5 * 4) {
				if (newy < bitmapHd5) {
					common.currPieceOnTouch = 3;
				} else if (newy < bitmapHd5 * 2) {
					common.currPieceOnTouch = 8;
				} else if (newy < bitmapHd5 * 3) {
					common.currPieceOnTouch = 13;
				} else if (newy < bitmapHd5 * 4) {
					common.currPieceOnTouch = 18;
				} else {
					common.currPieceOnTouch = 23;
				}
			} else {
				if (newy < bitmapHd5) {
					common.currPieceOnTouch = 4;
				} else if (newy < bitmapHd5 * 2) {
					common.currPieceOnTouch = 9;
				} else if (newy < bitmapHd5 * 3) {
					common.currPieceOnTouch = 14;
				} else if (newy < bitmapHd5 * 4) {
					common.currPieceOnTouch = 19;
				} else {
					common.currPieceOnTouch = 24;
				}
			}
		}

		if (event.getAction() == MotionEvent.ACTION_UP) {
			common.movingPiece = false;
			if (newx < bitmapWd5) {
				if (newy < bitmapHd5) {
					common.currSlotOnTouchUp = 0;
				} else if (newy < bitmapHd5 * 2) {
					common.currSlotOnTouchUp = 5;
				} else if (newy < bitmapHd5 * 3) {
					common.currSlotOnTouchUp = 10;
				} else if (newy < bitmapHd5 * 4) {
					common.currSlotOnTouchUp = 15;
				} else {
					common.currSlotOnTouchUp = 20;
				}
			} else if (newx < bitmapWd5 * 2) {
				if (newy < bitmapHd5) {
					common.currSlotOnTouchUp = 1;
				} else if (newy < bitmapHd5 * 2) {
					common.currSlotOnTouchUp = 6;
				} else if (newy < bitmapHd5 * 3) {
					common.currSlotOnTouchUp = 11;
				} else if (newy < bitmapHd5 * 4) {
					common.currSlotOnTouchUp = 16;
				} else {
					common.currSlotOnTouchUp = 21;
				}
			} else if (newx < bitmapWd5 * 3) {
				if (newy < bitmapHd5) {
					common.currSlotOnTouchUp = 2;
				} else if (newy < bitmapHd5 * 2) {
					common.currSlotOnTouchUp = 7;
				} else if (newy < bitmapHd5 * 3) {
					common.currSlotOnTouchUp = 12;
				} else if (newy < bitmapHd5 * 4) {
					common.currSlotOnTouchUp = 17;
				} else {
					common.currSlotOnTouchUp = 22;
				}
			} else if (newx < bitmapWd5 * 4) {
				if (newy < bitmapHd5) {
					common.currSlotOnTouchUp = 3;
				} else if (newy < bitmapHd5 * 2) {
					common.currSlotOnTouchUp = 8;
				} else if (newy < bitmapHd5 * 3) {
					common.currSlotOnTouchUp = 13;
				} else if (newy < bitmapHd5 * 4) {
					common.currSlotOnTouchUp = 18;
				} else {
					common.currSlotOnTouchUp = 23;
				}
			} else {
				if (newy < bitmapHd5) {
					common.currSlotOnTouchUp = 4;
				} else if (newy < bitmapHd5 * 2) {
					common.currSlotOnTouchUp = 9;
				} else if (newy < bitmapHd5 * 3) {
					common.currSlotOnTouchUp = 14;
				} else if (newy < bitmapHd5 * 4) {
					common.currSlotOnTouchUp = 19;
				} else {
					common.currSlotOnTouchUp = 24;
				}
			}

			// check for image to be in new slot
			if (common.currPieceOnTouch != common.currSlotOnTouchUp) {
				common.sendPieceToNewSlot(common.currPieceOnTouch,
						common.currSlotOnTouchUp);
				common.playSetSound();
			} else {
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.px = common.puzzleSlots[common.currSlotOnTouchUp].sx;
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.py = common.puzzleSlots[common.currSlotOnTouchUp].sy;
			}
		}

		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			common.movingPiece = true;
			if (common.currPieceOnTouch >= 0
					&& common.currPieceOnTouch < PIECES) {
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.px = newx
						- bitmapWd5 / 2;
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.py = newy
						- bitmapHd5 / 2;
			}
		}

		common.inPlace = 0;
		for (int i = 0; i < common.numberOfPieces; i++) {
			if (common.puzzleSlots[i].slotNum == common.puzzleSlots[i].puzzlePiece.pieceNum) {
				common.inPlace++;
			}
		}

		if (common.inPlace == common.numberOfPieces) {
			stopTimer();
			common.solved = true;
			common.showToast("Solve Time: " + getSolveTime());
			return false;
		}

		return true;
	}

	@Override
	public void recylceAll() {
		if (common.image != null)
			common.image.recycle();

		for (int i = 0; i < common.puzzlePieces.length; i++)
			if (common.puzzlePieces != null)
				if (common.puzzlePieces[i] != null)
					if (common.puzzlePieces[i].bitmap != null)
						common.puzzlePieces[i].bitmap.recycle();

	}

	@Override
	public String getPercentComplete() {
		return "" + (float) puzzleWork.piecesComplete / PIECES;
	}

	@Override
	public void initTimer() {
		puzzleWork.startPuzzle = new Date();
	}

	@Override
	public void stopTimer() {
		puzzleWork.stopPuzzle = new Date();
		puzzleWork.currPuzzleTime += puzzleWork.stopPuzzle.getTime()
				- puzzleWork.startPuzzle.getTime();
	}

	@Override
	public void resetTimer() {
		puzzleWork.currPuzzleTime = 0;
		puzzleWork.startPuzzle = new Date();
	}

	@Override
	public double getSolveTime() {
		return puzzleWork.currPuzzleTime / 1000.0;
	}

	@Override
	public int getCurrentImage() {
		// return current image index
		return puzzleWork.index;
	}

	@Override
	public void getPrevousImageLoadedScaledDivided(Thread thread) {
		// If the database returns with valid values the game can be resumed
		puzzleWork.newImageComplete = false;
		if (thread != null && thread.isAlive())
			thread.interrupt();

		thread = new Thread() {
			@Override
			public void run() {
				while (!puzzleWork.newImageComplete) {
					// get new index value and then remove index
					puzzleWork.index = common.currentPuzzleImagePosition;

					common.image = common.decodeSampledBitmapFromResource(
							common.res,
							Data.PICS[common.currentPuzzleImagePosition],
							common.screenW, common.screenH);

					common.image = Bitmap.createScaledBitmap(common.image,
							common.screenW, common.screenH, true);

					puzzleWork.newImageComplete = divideBitmapFromPreviousPuzzle();

					if (puzzleWork.newImageComplete) {
						resetTimer();
						common.errorLoading = false;
						common.solved = false;
						common.imageReady = true;
						common.mySoundPool.playChimeSound();
						System.gc();
					} else {
						common.errorLoading = true;
					}
				}
			}
		};
		thread.start();
	}

	@Override
	public void pause() {
		stopTimer();
	}

	@Override
	public void initPieces(int xSides, int ySides) {
		// TODO Auto-generated method stub
	}

	@Override
	public int getPieces() {
		return PIECES;
	}
}